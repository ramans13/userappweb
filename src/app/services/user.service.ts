import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';

import {  Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

   private apiUrl = "http://localhost:5000/user"
  constructor(private http : HttpClient) { }


getUsers() : Observable<any>{

   return this.http.get(this.apiUrl);
}

  addUser(user: Record<string, string>) : Observable<any>{
    const signupUrl = this.apiUrl+"/signup"
      return this.http.post(signupUrl,user)
  }

  login(data : Record<string,string>) :Observable<any>{
    const loginUrl = this.apiUrl+"/login"
       return this.http.post(loginUrl,data)
  }

  deleteUser(email:string):Observable<any>{
    const deleteUrl = `${this.apiUrl}/${email}`
    return this.http.delete(deleteUrl);
  }

}
