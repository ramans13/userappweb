import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './components/signup/signup.component';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';

const routes: Routes = [
  {path : 'signup',component:SignupComponent},
  {path : 'userdetail',component:UsersDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
