import { StringMapWithRename } from '@angular/compiler/src/compiler_facade_interface';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.css']
})
export class UsersDetailComponent implements OnInit {

  users!: any[]



  constructor(private userSer: UserService) { }

  ngOnInit(): void {

    this.userSer.getUsers().subscribe((value)=>{
      this.users = value;
    })
  }

  onClick(email: string ){
      this.userSer.deleteUser(email).subscribe((value)=>{
        // console.log(value);

          if(value.deletedCount === 1){
          this.users =  this.users.filter(user => email !== user.email)
            

          }

      })
  }

}
