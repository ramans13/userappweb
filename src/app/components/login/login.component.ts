import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email!: string;
  password!: string;

  loggedIn: boolean = false;

  constructor(private userSer: UserService, private Router: Router) { }

  ngOnInit(): void {
  }
  onSubmit() {

    if (!this.email || !this.password) {
      alert("Please enter details")
      return;
    }
    const data: Record<string, string> = { email: this.email, password: this.password }
    this.userSer.login(data).subscribe((value) => {
      // console.log(value)
      if (!value.isSuccessful) {
        alert(value.message);
      } else if (value.isSuccessful) {
        this.loggedIn = true;
      this.Router.navigateByUrl('/userdetail');

      }
    })
    this.email = "";
    this.password = ""


  }
}
