import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import{Router} from '@angular/router'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  Firstname!: string;
  Lastname!: string;
  Email!: string;
  Password!: string;
  ConPassword!: string
  user!: Record<string, string>
  constructor(private userSer: UserService , private router : Router) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (!this.Firstname || !this.Lastname || !this.Email || !this.Password || !this.ConPassword) {
      alert("All fieleds are mandatory.")
    } else if (this.Password !== this.ConPassword) {
      alert("Password did not match")
    } else {
      this.user = {

        firstname: this.Firstname,
        lastname: this.Lastname,
        email: this.Email,
        password: this.Password
      }
      this.userSer.addUser(this.user).subscribe((value)=>{

          alert(value.message)
          this.router.navigateByUrl('/')

      })

      this.Firstname="";
      this.Lastname="";
      this.Email="";
      this.Password="";
      this.ConPassword=""

    }
  }

}
